"use strict";

appStartWars.controller('indexCtrl', ["$scope", "config", "IndexService", function($scope, config, IndexService) {

   $scope.personagens = [];
   $scope.exibir = false;

    
    $scope.AbsentEmployees = {};
    var abEmployees = $scope.AbsentEmployees;
    abEmployees.dataLoaded = false;

   $scope.getPersonagens = function() {
    IndexService.getPersonagens(config).success(
        function(data){
            $scope.personagens = data;
            console.log(data);
            abEmployees.dataLoaded = true;
        }).error(
        function(data, status, headers, config) {
            $rootScope.showError(Scopes.get('myHttpInterceptor').messageError);
        });
    }

    $scope.exibirDetalhes = function(personagem){
        $scope.pers = personagem;
        $scope.exibir = true;
    }

    $scope.voltar = function(){
        $scope.exibir = false;
        $scope.getPersonagens();        
    }

    $scope.getPersonagens();
}]);