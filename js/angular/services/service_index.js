"use strict";

appStartWars.factory("IndexService", function($http){

	var _getPersonagens = function(config){
		return $http.get("https://swapi.co/api/people");
	}

	return {
		getPersonagens : _getPersonagens,
	};
});