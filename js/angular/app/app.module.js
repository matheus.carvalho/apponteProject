"use strict";

var appStartWars = angular.module("appStartWars", ["angular-growl","angular-notification-icons", "ui.router", "angularUtils.directives.dirPagination"])
    .config(["$stateProvider", "$httpProvider", "$urlRouterProvider", function($stateProvider, $httpProvider, $urlRouterProvider) {

        /** HTTP Inteceptor **/
      //  $httpProvider.interceptors.push('myHttpInterceptor');

        /** Routes **/
        $stateProvider

            .state('index', {
            url: '/index',
            views: {
                "main": {
                    controller: 'indexCtrl',
                    templateUrl: 'views/index.html'
                }
            }
        })


        $urlRouterProvider.otherwise('/index');
    }]);

appStartWars.run(["$rootScope", "$http", "growl", "$interval",
    function($rootScope, $http, growl, $interval) {

        /** Configuração das mensagens de alerta **/
        $rootScope.showWarning = function(mensagem) {
            growl.warning(mensagem, { title: 'Atenção!', ttl: 5000, disableCountDown: true });
        }

        $rootScope.showError = function(mensagem) {
            growl.error(mensagem, { title: 'Algo deu errado!', ttl: 5000, disableCountDown: true });
        }

        $rootScope.showSuccess = function(mensagem) {
            growl.success(mensagem, { title: 'Tudo certo!', ttl: 5000, disableCountDown: true });
        }

        $rootScope.showInfo = function(message) {
            growl.info(message, { title: 'Informação!', ttl: 5000, disableCountDown: true });
        }

        $rootScope.showSuccessMov = function(message) {
            growl.success(message, { title: 'Informação!', ttl: 15000, disableCountDown: true });
        }
    }
]);